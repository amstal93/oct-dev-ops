terraform {
  required_version = "= 1.0.0"
  backend "s3" {
    region         = "us-east-2"
    bucket         = "oct-terraform-state"
    key            = var.key
    dynamodb_table = "oct-terraform-state"
    encrypt        = true
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      prefix = var.prefix
      user   = var.user
    }
  }
}