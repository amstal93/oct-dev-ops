variable "region" {
  type = string
}

variable "prefix" {
  type = string
}

variable "user" {
  type = string
}