data "aws_route53_zone" "hosted_zone" {
  count        = var.dns ? 1 : 0
  name         = var.domain
  private_zone = false
}

resource "aws_acm_certificate" "cert" {
  count             = var.dns ? 1 : 0
  domain_name       = "*.${var.domain}"
  validation_method = "DNS"
}

resource "aws_route53_record" "cert_validation_recordset" {
  for_each = {
    for dvo in(var.dns ? aws_acm_certificate.cert[0].domain_validation_options : []) : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.hosted_zone[0].zone_id
}

resource "aws_acm_certificate_validation" "cert_validation" {
  count                   = var.dns ? 1 : 0
  certificate_arn         = aws_acm_certificate.cert[0].arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation_recordset : record.fqdn]
}

resource "aws_route53_record" "record" {
  count   = var.dns ? 1 : 0
  zone_id = data.aws_route53_zone.hosted_zone[0].zone_id
  name    = var.prefix
  type    = "CNAME"
  ttl     = "5"
  records = [aws_lb.alb.dns_name]
}