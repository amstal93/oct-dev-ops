resource "aws_cloudwatch_log_group" "ecs_log_group" {
  name              = "/ecs/${var.prefix}/"
  retention_in_days = 14
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.prefix}-ecs-cluster"
}

data "aws_ecr_repository" "ecr_oct" {
  name = "${var.prefix}/oct"
}

data "template_file" "task_def" {

  template = file("${path.module}/task.json")
  vars = {
    PREFIX           = var.prefix
    IMAGE            = "${data.aws_ecr_repository.ecr_oct.repository_url}:${var.pipeline_id}"
    REGION           = var.region
    ORCHARD_APP_DATA = var.orchard_app_data
  }

}

resource "aws_ecs_task_definition" "site_task_def" {
  family                   = "${var.prefix}-site-task-def"
  container_definitions    = data.template_file.task_def.rendered
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  memory                   = 2048
  cpu                      = 1024
  execution_role_arn       = aws_iam_role.ecs_task_role.arn
  task_role_arn            = aws_iam_role.ecs_task_role.arn

  volume {
    name = "efs"
    efs_volume_configuration {
      file_system_id     = var.efs_id
      transit_encryption = "ENABLED"
    }
  }

}

resource "aws_ecs_service" "ecs_site_service" {
  name                    = "${var.prefix}-site-ecs-service"
  cluster                 = aws_ecs_cluster.ecs_cluster.id
  task_definition         = aws_ecs_task_definition.site_task_def.arn
  launch_type             = "FARGATE"
  desired_count           = 1
  enable_ecs_managed_tags = true
  enable_execute_command  = true
  force_new_deployment    = true

  network_configuration {
    subnets          = data.aws_subnet_ids.subnets.ids
    security_groups  = [aws_security_group.ecs_sg.id]
    assign_public_ip = true # for now, this will eventually go in private subnets
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.site_target_group.arn
    container_port   = 5000
    container_name   = "${var.prefix}-site" // matches task definition
  }

  depends_on = [
    data.aws_iam_policy.AmazonECSTaskExecutionRolePolicy,
    aws_lb_listener.http_listener
  ]
}