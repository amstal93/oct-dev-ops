# Orchard Core Transformalize (oct) DevOps

This is GitLab CI/CD and Terraform for deploying [OrchardCore.Transformalize](https://github.com/dalenewman/OrchardCore.Transformalize) into AWS ECS (Fargate).

It requires AWS credententials set in *Settings > CI/CD > Variables*:

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

There are three jobs in the pipeline: State, Bootstrap, and Site.  They run in that order; deploying first, and leaving the option to destroy.

## The State Job
This refers to Terraform's backend state.  It tracks the state *of the infrastructure*.  It uses an S3 bucket and DynamoDb table.  It is created with a small [CloudFormation](https://aws.amazon.com/cloudformation) [stack](bootstrap/state.yml).  It runs on the *main* branch, but other branches reuse it by passing in a unique `key` parameter.

## The Bootstrap Job
Bootstrap contains anything you need to create in advance.  For example, if you we're building docker images instead of pull one from Docker Hub, you could create an ECR (Elastic Container Registry) first.  Then you'd have somewhere private to push your images.

## The Site Job
This stack houses the main infrastructure; which in this case is a web site.  It uses Fargate style ECS (Elastic Container Service), ALB (Application Load Balancer), and ACM and Route53 to provide a friendly https address.  The address follow this format: `https://<branch-name>.<domain>`. 

## About Orchard Core
[Orchard Core](https://www.orchardcore.net) is a CMS (Content Management System) built on asp.net core. It needs a persistent file system outside the container. This repo uses EFS (Elastic File System) to provide a file system for Orchard's *App_Data* folder.

When the *App_Data* folder is empty, Orchard Core starts up at a setup page.  Setup requires a relational database connection string, so you'll need to have an RDS (Relational Database Service) database readily available.

With EFS and RDS, you'll need to allow incoming connections to their security groups. To avoid Terraform destruction issues, allow connections from the subnets in your VPC (rather than the ECS task security group itself).  In the future, RDS and EFS will be in the stack allowing it to specify incoming connections from ECS (instead of the whole subnet).
